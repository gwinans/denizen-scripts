# random_death_messages script by GoMinecraft
#
# This script will replace the default MC death messages with custom ones!
# Add your own!
#
# Huge thanks to the Denizen Discord gang for putting up with my brain failure moments.
#
# Inspired by my own DeathNotifier Spigot plugin and the RandomDeathMessages Denizen script by Mwthorn
#

random_death_messages:
  type: world
  debug: false
  events:
    on player death:
    - if <context.cause> == ENTITY_ATTACK:

      # Here we have PvP. Interesting things can be done here eventually.
      # TODO:
      # * Add item in hand
      # * Figure out who shot the arrow, right now only comes back as a Projectile.
      - if <context.damager.entity_type> == PLAYER:
        - determine "<green><player.name> <white>was killed by <red><context.damager.name><white>!"

      # Monster death types
      - choose <context.damager.entity_type>:
        - case BLAZE:
          - random:
            - determine "<green><player.name> <white>was set ablaze by a <red>Blaze"
            - determine "<green><player.name> <white>is smokin'. Needs to avoid the <red>Blaze<white>."
            - determine "<green><player.name> <white>got flamed by a <red>Blaze<white>!"
        - case CAVE_SPIDER:
          - random:
            - determine "<green><player.name> <white>dug deep. Got turned into a snack by a <red>Cave Spider<white>."
            - determine "<white>The <red>Cave Spiders <white>are going to eat well tonight thanks to <green><player.name><white>."
        - case CREEPER:
          - random:
            - determine "<green><player.name> <white>got blown to bits by a <red>Creeper<white>!"
            - determine "<green><player.name> <white>was blown up by a <red>Creeper"
            - determine "<white>All the <red>Creeper <white>wanted was a hug. <green><player.name> <white>foolishly obliged."
            - determine "<green><player.name> <white>discovered that '<red>TsssSSsSSs<white>' ends in an explosion."
            - determine "<green><player.name> <white>had a run-in with a <red>Creeper<white>. They are now here. And there.. oh, and over there."
        - case DROWNED:
          - random:
            - determine "<green><player.name> <white>met the <red>Drowned<white>. First impression didn't go well."
            - determine "<green><player.name> <white>got dragged down by the <red>Drowned<white>."
        - case ELDER_GUARDIAN:
          - random:
            - determine "<green><player.name> <white>was put down by an <red>Elder Guardian<white>."
            - determine "<white>Eyelasers hurt, <green><player.name><white>. Stay away from the <red>Elder Guardian<white>."
        - case ENDER_DRAGON:
          - random:
            - determine "<green><player.name> <white>met the <red>Lord of the End<white>. The End."
        - case ENDERMAN:
          - random:
            - determine "<white>It's not nice to stare, <green><player.name><white>, especially at <red>Endermen<white>."
        - case ENDERMITE:
          - random:
            - determine "<white>A nasty little <red>Endermite <white>nibbled <green><player.name><white>'s feet off.'"
        - case EVOKER_FANGS:
          - random:
            - determine "<green><player.name> was put to death by an <red>Evoker<white>."
        - case GHAST:
          - random:
            - determine "<green><player.name> <white>died from <red>Ghast <white>exposure."
            - determine "<white>A ghastly <red>Ghast <white>left <green><player.name> <white>aghast."
            - determine "<green><player.name> <white>got <red>fireball'd <white>by a <red>Ghast<white>!"
            - determine "<green><player.name> <white>got gibbed by a <red>Ghast<white>!"
            - determine "<green><player.name> <white>has been owned by the all-powerful <red>Ghast<white>!"
        - case GUARDIAN:
          - random:
            - determine "<green><player.name> <white>was put down by an <red>Guardian<white>."
            - determine "<white>Eyelasers hurt, <green><player.name><white>. Stay away from the <red>Guardian<white>."
        - case HUSK:
          - random:
            - determine "<white>A husky <red>Husk <white>husked <green><player.name><white>."
        - case IRON_GOLEM:
          - random:
            - determine "<green><player.name> <white>broke the village rules and got clobbered by a <red>Golem<white>."
            - determine "<green><player.name> <white>tangled with a <red>Golem<white> and got smashed to bits."
        - case LLAMA:
          - random:
            - determine "<green><player.name> <white>discovered that a <red>Llama <white>can spit violently.."
        - case MAGMA_CUBE:
          - random:
            - determine "<green><player.name> <white>was slimed by a <red>Magma Cube<white>."
            - determine "<white>A happy, bouncing <red>Lava Blob<white> got up close and personal with <green><player.name><white>."
        - case PIG_ZOMBIE:
          - random:
            - determine "<green><player.name> <white>angered a <red>Ham Shambler<white>. It didn't end it their favor."
            - determine "<green><player.name> <white>did not respect the <red>Pig Zombie<white>."
        - case PILLAGER:
          - random:
            - determine "<green><player.name> <white>got pillaged by a <red>Pillager<white>."
        - case POLAR_BEAR:
          - random:
            - determine "<green><player.name> <white>got mauled by a <red>Polar Bear<white>."
        - case SKELETON:
          - random:
            - determine "<white>A <red>Skeleton <white>sniped <green><player.name><white>."
            - determine "<green><player.name> <white>got ventilated by a <red>Skeleton<white>!"
            - determine "<white>A <red>Skeleton <white>had a bone to pick with <green><player.name><white>."
            - determine "<green><player.name> <white>was beaten by a pile of bones holding a bow. Pathetic."
        - case SHULKER:
          - random:
            - determine "<green><player.name> <white>was put down by a <red>Shulker<white>."
        - case SILVERFISH:
          - random:
            - determine "<green><player.name> <white>was nibbled to death by a <red>Silverfish<white>."
            - determine "<green><player.name> got their ankles nibbled off by a <red>SilverFish<white>."
        - case SLIME:
          - random:
            - determine "<green><player.name> <white>was slimed by a <red>Slime<white>."
            - determine "<green><player.name> <white>was removed from the gene-pool by a <red>Slime<white>!"
            - determine "<green><player.name> <white>got <red>Slimed<white>!"
            - determine "<green><player.name> <white>discovered that <red>Slime <white>is not something you wear."
            - determine "<green><player.name> <white>got too close to the <red>Slime<white>!"
            - determine "<green><player.name> <white>was absorbed by the <red>Slime<white>!"
        - case SPIDER:
          - random:
            - determine "<green><player.name> <white>got munched on by a <red>Spider<white>."
            - determine "<green><player.name> <white>fell victim to a giant, hungry <red>Spider<white>!"
            - determine "<green><player.name> <white>just had a nasty encounter with a not so itsy bitsy <red>Spider<white>."
            - determine "<green><player.name> <white>had a run in with a giant <red>Spider<white>... it didn't go well."
        - case VEX:
          - random:
            - determine "<white>An <red>Evoker<white>'s tiny little friend killed <green><player.name><white>."
            - determine "<green><player.name> <white>was <red>Vexed <white>by an <red>Evoker<white>'s pet."
        - case WITCH:
          - random:
            - determine "<green><player.name> <white>is no match for <red>witchcraft<white>."
            - determine "<green><player.name> <red>got hexed by an upset <red>Witch<white>!"
        - case WITHER_SKELETON:
          - random:
            - determine "<green><player.name> <white>was destroyed by a <red>Wither<white>!"
            - determine "<green><player.name> <white>withered under the onslaught of a <red>Wither<white>."
        - case ZOMBIE:
          - random:
            - determine "<green><player.name> <white>needs to plant more vegetation against the <red>Zombie <white>invasion."
            - determine "<green><player.name> <white>got their brains munched on by a <red>Zombie<white>."
            - determine "<green><player.name> <white>became dinner for a hungry <red>Zombie<white>!"
            - determine "<green><player.name> <white>is now missing <red>bbrrraaaaiinnnnsss<white>."
            - determine "<green><player.name> <white>just gave their brain to a <red>Zombie<white>."
            - determine "<green><player.name> <white>donated their brain to science. Or maybe it was a <red>Zombie<white>."
        - case ZOMBIE_VILLAGER:
          - random:
            - determine "<green><player.name> <white>got debrained by a <red>Zombie Villager<white>!"
            - determine "<green><player.name> <white>discovered the raised dead like brain meats."

        # No idea how to avoid repeating this block
        - default:
          - debug log "Report the below line to the developer!"
          - debug log "Cause: <context.damager.entity_type>"

    - else:
      # Environment death types
      - choose <context.cause>:
        - case CONTACT:
          - random:
            - determine "<green><player.name> <white>hugged a <red>Cactus<white>. Not smart."
            - determine "<green><player.name> <white>came into contact with something very prickly."
        - case CRUSHED:
          - random:
            - determine "<green><player.name> <white>was squashed."
        - case DROWNING:
          - random:
            - determine "<white>Gurgle gurgle, blub blub. These were <green><player.name><white>'s final words."
            - determine "<green><player.name> <white>forgot they haven't evolved gills yet."
        - case FALL:
          - random:
            - determine "<green><player.name> <white>answered the call of <red>gravity<white>."
            - determine "<green><player.name> <white>forgot to bring a parachute."
            - determine "<green><player.name> <white>did not fly, just fell with style."
            - determine "<white>It's not the fall that kills you, <green><player.name><white>. It's the sudden stop at the end."
            - determine "<green><player.name> <white>did indeed bumble and took quite a tumble."
            - determine "<green><player.name> <white>broke more than their legs in that jump."
        # This appears to only happen due to an anvil falling.
        - case FALLING_BLOCK:
          - random:
            - determine "<green><player.name> <white>tried to wear an <red>Anvil <white>. It was too heavy."
            - determine "Hey, <green><player.name><white>. <red>Anvils <white>don't belong on your head."
        - case FIRE:
        - case FIRE_TICK:
          - random:
            - determine "<green><player.name> <red>burned <white>to death."
            - determine "<white>Can you smell what The Rock is cookin', <green><player.name><white>? It's you!"
            - determine "<green><player.name> <white>died in a fire."
            - determine "<green><player.name> <white>was cremated."
            - determine "<green><player.name> <white>is quite flammable, tests show."
            - determine "<green><player.name> <white>was incinerated."
        - case FLY_INTO_WALL:
          - random:
            - determine "<green><player.name> <white>flew into a wall, got turned into a thin streak of goo."
            - determine "<green><player.name> flew too close to the su.. er, that wall."
        - case HOT_FLOOR:
          - random:
            - determine "<green><player.name> got nice and toasty on a <red>Magma Block<white>."
        - case LAVA:
          - random:
            - determine "<green><player.name> <white>took a <red>Lava <white>bath."
            - determine "<green><player.name> <white>discovered just how hot <red>Lava <white>really is."
            - determine "<white>Bathing in <red>Lava <white> is not how you get clean, <green><player.name><white>."
        - case LIGHTNING:
          - random:
            - determine "<green><player.name> <white>was given a <red>shocking <white>revelation."
            - determine "<green><player.name> <white>has heard of <red>shock humour<white>, but this is ridiculous."
            - determine "<white>Zeus was displeased with <green><player.name><white>."
            - determine "<green><player.name> <white>now understands what an electrifying experience being outside in a storm can be."
        - case POISON:
          - random:
            - determine "<green><player.name> <white>was <red>poisoned <white>to death."
            - determine "<white>There was no antidote for <green><player.name><white>. They died of <red>poisoning<white>."
        - case PRIMED_TNT:
          - random:
            - determine "<green><player.name> <white>blew up with the help of some <red>TNT<white>."
            - determine "<green><player.name> <white>stood too lose to <red>TNT<white>."
        - case STARVATION:
          - random:
            - determine "<green><player.name> <red>starved <white>to death."
            - determine "<green><player.name> needs to eat a sammich, died of <red>starvation<white>."
        - case SUFFOCATION:
          - random:
            - determine "<green><player.name> <red>suffocated."
            - determine "<green><player.name> <white>forgot how to breath."
            - determine "<green><player.name> <white>ran out of air."
        - case SUICIDE:
          - random:
            - determine "<green><player.name> <white>has been slain by <green><player.name><white>."
            - determine "<green><player.name> <white>committed sudoku. Er, wait. I meant <red>suicide<white>."
        - case VOID:
          - random:
            - determine "<green><player.name> <white>found the <red>Void<white>."
            - determine "<white>You need to avoid the <red>Void<white>, <green><player.name><white>."
            - determine "<green><player.name> <white>fell into the <red>Void<white>!"
            - determine "<green><player.name> <white>discovered that the <red>Void <white>really is endless.'"
            - determine "<green><player.name> <white>fell through the bottom of the world. Wow."
            - determine "<green><player.name> <white>embraced eternity."
            - determine "<green><player.name> <white>jumped to infinity and beyond."
            - determine "<green><player.name> <white>fell into the void."
            - determine "<green><player.name> <white>explored the great beyond."
            - determine "<green><player.name> <white>went exploring in the <red>Void<white>."

        # How do I avoid repeating this block?
        - default:
          - debug log "Report the below line to the developer!"
          - debug log "Cause: <context.cause>"

      # Default message.
      - determine "<context.message>"
